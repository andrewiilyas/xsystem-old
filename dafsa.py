import operator
import string

import numpy.random as rand
import numpy as np
from scipy.stats import chisquare

import rl_config
from utils import Utils

import itertools


class Layer:

    def __init__(self, node):
        self.compression_accuracy = rl_config.get("capture_percentage")
        self.nodes = {node: 1}
        self.symbols = {Utils.get_class(node): [node]}
        self.total_count = 1
        self.optional = False
        self.compressed = None

    def compress(self):
        symbol_pcts = {}
        # Compute percentages
        max_symbol = None
        max_symbol_pct = 0
        for symbol in self.symbols:
            symbol_pcts[symbol] = float(
                len(self.symbols[symbol])) / self.total_count
            if symbol_pcts[symbol] > max_symbol_pct:
                max_symbol = symbol
                max_symbol_pct = symbol_pcts[symbol]

        # Case 1: all the same character
        # Does the character itself matter?
        if max_symbol_pct > 0.9:
            is_invalid_character = (
                ord(max_symbol) < 48 or ord(max_symbol) > 122)
            if (is_invalid_character or
                    ord(max_symbol) == Utils.get_ord(max_symbol)):
                return max_symbol
            else:
                alphabet = [0] * 26
                if max_symbol == 'D':
                    alphabet = [0] * 10
                alphabet_dict = {}
                for c in self.symbols[max_symbol]:
                    try:
                        alphabet[Utils.get_ord(c)] += 1
                        alphabet_dict[c] = alphabet_dict.get(c, 0) + 1
                    except IndexError:
                        continue
                p_val = chisquare(sorted(alphabet)).pvalue
                if not p_val <= 0.05:#10**(-10):
                    return max_symbol
                else:
                    comp_acc = 0.0
                    my_regex = ""
                    while comp_acc < self.compression_accuracy:
                        this_symb = max(alphabet_dict.items(),
                                        key=operator.itemgetter(1))[0]
                        my_regex += this_symb + "|"
                        comp_acc += float(alphabet_dict[this_symb]
                                          ) / float(sum(alphabet))
                        alphabet_dict[this_symb] = 0
                    re = my_regex[:-1]
                    if "|" in re:
                        return "(" + re + ")"
                    return re
        else:
            alphabet = [0] * 256
            for c in self.symbols[max_symbol]:
                    alphabet[Utils.get_ord(c)] += 1
            p_val = chisquare(alphabet).pvalue
            if p_val > 0.01:
                return max_symbol
            else:
                return "*"

    def acceptable_letters(self):
        compressed = self.compress()
        if compressed == '*':
            acceptable_letters = list(string.letters + string.digits)
        elif compressed == 'D':
            acceptable_letters = list(string.digits)
        elif compressed == 'C':
            acceptable_letters = list(string.ascii_uppercase)
        elif compressed == 'c':
            acceptable_letters = list(string.ascii_lowercase)
        elif "|" in compressed:
            acceptable_letters = compressed[1:-1].split("|")
        else:
            acceptable_letters = [compressed]
        return acceptable_letters


    def generate_letter(self):
        return rand.choice(self.acceptable_letters())

    def add_node(self, node, dry_run=False):
        added_new = 0
        if not dry_run:
            self.nodes[node] = self.nodes.get(node, 0) + 1
        if not Utils.get_class(node) in self.symbols:
            added_new = 1
        elif node not in self.nodes and not self.compress() in {'*', 'D', 'C', 'c'}:
            added_new = 0.5
        if not dry_run:
            self.symbols[Utils.get_class(node)] = self.symbols.get(
                Utils.get_class(node), []) + [node]
            self.total_count += 1
        return added_new

    def letter_reps(self):
        compressed = self.compress()
        if "|" in compressed:
            return compressed[1:-1].split("|")
        else: # compressed in {"*", "D", "C", "c"} or single char
            return [compressed]

    def __repr__(self):
        if self.optional:
            return "[" + self.compress() + "]"
        else:
            return self.compress()


class DAFSA():

    def __init__(self):
        self.layers = []
        self.score = 0
        self.historical_score = [0]
        self.done_adding = False
        self.inc = 10
        self.std_score = 100
        self.n = rl_config.config["clt_sample_size"]
        self.needed_sample_size = lambda x: (1.96 * x/0.5)**2
        self.mean_score = None
        self.old_hist_score = [0]


    def generate_letters(self):
        letters_arr = [l.generate_letter() for l in self.layers]
        return ''.join(letters_arr)

    def add_word(self, word):
        if not self.done_adding:
            counter = 0
            while counter < len(word):
                if len(self.layers) < counter + 1:
                    new_layer = Layer(word[counter])
                    self.layers.append(new_layer)
                else:
                    self.score += self.layers[counter].add_node(word[counter])
                counter += 1
            self.historical_score.append(self.score)
            if len(self.historical_score) % self.inc == 0:
                self.std_score = np.std(self.historical_score)
            if self.needed_sample_size(self.std_score) < len(self.historical_score):
                self.done_adding = True
                self.historical_score = self.historical_score + self.old_hist_score
                self.mean_score = np.mean(self.historical_score)

    def allow_adding(self):
        self.old_hist_score = self.historical_score
        self.historical_score = [0]
        self.done_adding = False

    def make_representation(self):
        my_string = ""
        for layer in self.layers:
            my_string += layer.compress()
        return my_string

    def __repr__(self):
        return self.make_representation()

    def __str__(self):
        return self.make_representation()

    def to_expression(self):
        return self.make_representation()

    def feed_forward(self, word):
        score = 0
        counter = 0
        while counter < len(word):
            if len(self.layers) < counter + 1:
                score += 1
            else:
                score += self.layers[counter].add_node(
                    word[counter], dry_run=True)
            counter += 1
        score += (1 if len(self.layers) > counter else 0)
        #score += max(len(self.layers) - counter, 0)
        return score

    def all_valid_words(self):
        valid_per_letter = [l.letter_reps() for l in self.layers]
        words = []
        for prod_string in itertools.product(*valid_per_letter):
            words.append(''.join(prod_string))
        return words
