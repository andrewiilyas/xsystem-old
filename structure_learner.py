import string
from copy import deepcopy

import numpy.random as rand

import rl_config
from dafsa import DAFSA
from utils import Utils

import itertools

class StructuredTextLearner:

    def __init__(self, lines):
        self.lines = lines
        self.hinges = self.find_hinges()
        self.ds = [DAFSA() for _ in range(len(self.hinges) + 1)]
        self.last_state = None
        self.regex = None
        self.learned_lines = deepcopy(lines)

    # Used for performance plots.
    def get_num_hinges(self):
        return len(self.hinges)

    def random_learned_string(self):
        return self.learned_lines[0]

    def generate_random_strings(self, n):
        for _ in range(n):
            cur_string = ""
            for j in range(len(self.ds)):
                cur_string += self.ds[j].generate_letters()
                cur_string += (self.hinges + [""])[j]
            yield cur_string

    def generate_mh_strings(self):
        list_of_valids = []
        for i in range(len(self.ds)):
            list_of_valids.append(self.ds[i].all_valid_words())
        for valid in itertools.product(*list_of_valids):
            curr_string = ""
            for j in range(len(valid)):
                curr_string += valid[j]
                curr_string += (self.hinges + [""])[j]
            yield curr_string

    def learn_string(self, line):
        self.learned_lines.append(line)
        current_line = line
        split_string = []
        for h in self.hinges:
            split_line = current_line.split(h)
            split_string.append(split_line[0])
            current_line = current_line[len(split_line[0]) + 1:]
        split_string.append(current_line)
        [self.ds[i].add_word(split_string[i]) for i in range(len(self.ds))]

    def open_for_learning(self):
        for d in self.ds:
            d.allow_adding()

    def compute_regex(self):
        new_hinges = self.hinges + ['']
        self.regex = [str(self.ds[i]) + new_hinges[i]
                      for i in range(len(self.ds))]

    def learn_deep_structure(self):
        hinges = self.hinges
        for line in self.lines:
            self.learn_string(line)
        new_hinges = hinges + ['']
        self.regex = [str(self.ds[i]) + new_hinges[i]
                      for i in range(len(self.ds))]
        return True

    def find_hinges(self):
        n = rl_config.get("default_sample_size")(self.lines) + 1
        doc_strings = list(rand.choice(self.lines, n, replace=False))
        candidates = list(string.whitespace + string.punctuation)
        hinge_list = [''.join([x for x in ds if x in candidates])
                      for ds in doc_strings] + ['']
        hinges = list(Utils.mode(hinge_list))
        return hinges

    def feed_forward_score(self, lines=None, num_lines=None):
        hinges = self.hinges
        s = 0
        avg_line_length = 0
        for line in lines:
            line = str(line)
            avg_line_length += len(line)
            current_line = line
            split_string = []
            for h in hinges:
                split_line = current_line.split(h)
                split_string.append(split_line[0])
                current_line = current_line[len(split_line[0]) + 1:]
            split_string.append(current_line)
            s += float(sum([self.ds[i].feed_forward(split_string[i])
                            for i in range(len(self.ds))]))
        s /= avg_line_length
        return s

    # String representation
    def __str__(self):
        self.compute_regex()
        return ''.join(self.regex)
