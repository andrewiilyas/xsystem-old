import argparse
import csv
import operator
import pandas as pd
import re


def compare_regex_to_column(column_df_chunk, regex, category_label):
    compiled_regex = None
    try:
        compiled_regex = re.compile(regex)
    except:
        # TODO: save this to some output "invalid_regexes" file.
        # print 'invalid regex: %s' % regex
        return

    # Try and match all entries in the column against the regex.
    num_rows = 0
    num_matches = 0

    for row in column_df_chunk:
        num_rows += 1
        cell_value = str(row)
        result = compiled_regex.match(cell_value)
        if result is not None:
            # There is match, print it.
            num_matches += 1

    fraction_matches = (num_matches * 1.0) / (num_rows * 1.0)
    return fraction_matches


def get_all_regexes(regexes_csv_file):
    """
        Reads all entries from a regexes CSV file with (label,regex) pairs.
    """
    df = pd.read_csv(regexes_csv_file)
    return df


def process_dataset(dataset_csv_file, dataset_csv_delimiter, regexes_df, output_file):
    """
        Processes all columns in the input CSV data set.
    """

    # Heuristic: otherwise it takes way too long.
    max_rows_per_column = 1000
    with open(output_file, 'w') as output_csv_file:
        csv_fields = ['file_name', 'column_name', 'label', 'regex']
        writer = csv.DictWriter(
            output_csv_file, fieldnames=csv_fields, quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writeheader()

        # Read data set incrementally, becase it could be HUGE.
        num_rows_per_read = 100000
        iter_csv = pd.read_csv(
            dataset_csv_file, delimiter=dataset_csv_delimiter, iterator=True, chunksize=num_rows_per_read)
        label_frequency_per_column = {}
        regexes = {}
        for chunk_df in iter_csv:
            for column in chunk_df:
                df_without_na = chunk_df[column].dropna()

                if len(df_without_na) is 0:
                    print 'No rows left after dropping NaN. Skipping \"%s\"' % column
                    continue

                print 'Computing label candidates for %s...' % column
                if len(df_without_na) > max_rows_per_column:
                    print 'Using %d rows sample.' % max_rows_per_column
                    df_without_na = df_without_na.sample(max_rows_per_column)

                result = compute_label_for_chunk(df_without_na, regexes_df)
                label = result['label']
                regexes[label] = result['regex']
                print 'Assigning label \"%s\"' % label

                if (column, label) not in label_frequency_per_column:
                    label_frequency_per_column[(column, label)] = 0
                label_frequency_per_column[(column, label)] = label_frequency_per_column[
                    (column, label)] + 1

        # After processing all row chunks, we have a map of label frequencies:
        # ('column', 'label') -> frequency
        #
        # And we assign the most frequent label to that column.
        most_frequent_labels = sorted(label_frequency_per_column.items(),
                                      key=operator.itemgetter(1))
        most_frequent_labels.reverse()
        final_labels = {}
        for column_label, frequency in iter(most_frequent_labels):
            column_name = column_label[0]
            label_name = column_label[1]
            # Reverse sorted order guarantees we get most frequent label.
            if column_name not in final_labels:
                final_labels[column_name] = label_name
                label_regex = regexes[label_name]
                row = {'file_name': dataset_csv_file,
                       'column_name': column_name,
                       'label': label_name,
                       'regex': label_regex}
                writer.writerow(row)


def update_regex_if_smaller(label_regex, label, new_regex):
    """
        Updates current regex for label if new regex is smaller.
    """
    if label not in label_regex:
        label_regex[label] = new_regex
        return

    if len(new_regex) < len(label_regex[label]):
        label_regex[label] = new_regex


def compute_label_for_chunk(column_df_chunk, regexes_df):
    """
        Processes single column in the input CSV data set.
    """
    label_matches = {}
    label_smallest_regex = {}
    max_regex_length = 50
    for _, regex_row in regexes_df.iterrows():
        category_label = regex_row['category']
        if category_label not in label_matches:
            label_matches[category_label] = 0

        # Heuristic: if the regex is too long, we skip it.
        regex = regex_row['regex']
        if len(regex) > max_regex_length:
            continue

        fraction_matches = compare_regex_to_column(
            column_df_chunk, regex, category_label)

        # We store only perfect matches.
        if fraction_matches > 0.99:
            label_matches[category_label] += regex_row['weight']
            # We don't care which regex, so long as it is a 100% perfect match.
            update_regex_if_smaller(
                label_smallest_regex, category_label, regex)

#  We ignore approximate matches for now, since we have lots of 100% matches.
#
#        elif fraction_matches > 0.1 and fraction_matches < 0.9:
#            print 'Approximate match of %f%% for \"%s\" regex: \"%s\"' % (
#                fraction_matches * 100.0, category_label, regex)

    top_label = get_top_label(label_matches)
    if top_label is None:
        return None

    return {'label': top_label, 'regex': label_smallest_regex[top_label]}


def get_top_label(label_matches):
    """
        Sorts labels by number of 100% matches, and returns top label.
    """
    sorted_label_matches = sorted(
        label_matches.items(), key=operator.itemgetter(1))
    sorted_label_matches.reverse()

    top_label = None
    for label, _ in iter(sorted_label_matches):
        top_label = label
        break

    return top_label

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Find candidate regexes for a given value.')
    parser.add_argument(
        '-r',
        '--regexes_input_csv',
        help='Relative path of input CSV file containing scraped regexes with labels.',
        required='True')
    parser.add_argument(
        '-d',
        '--dataset_input_csv',
        help='Relative path of input dataset CSV file.',
        required='True')
    parser.add_argument(
        '-o',
        '--output_csv',
        help='Relative path of output labels CSV file.',
        required='True')
    parser.add_argument(
        '-c',
        '--dataset_column_delimiter',
        help='Character to be used as delimiter in the input dataset CSV file.')
    args = parser.parse_args()

    regexes_df = get_all_regexes(args.regexes_input_csv)
    process_dataset(args.dataset_input_csv,
                    args.dataset_column_delimiter,
                    regexes_df,
                    args.output_csv)

    print 'Output saved at \"%s\"' % args.output_csv
