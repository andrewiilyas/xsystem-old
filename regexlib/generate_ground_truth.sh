#!/usr/local/bin/bash
#
# Requires Bash v4 for associative arrays.
# Mac install with Homebrew will make it available under /usr/local/bin.

# Globals.
TODAY=`date "+%Y%m%d"`
# Assumes we're running from regexlib/ directory.
#INPUT_REGEXES=regexlib_regexes.csv
INPUT_REGEXES=weighted_regexes.csv  # Less "strings" regexes.

# Dataset -> directory
declare -A INPUT_DATASETS
#INPUT_DATASETS["sample"]=sample_datasets/*.csv
INPUT_DATASETS["chemical"]=/data/datasets/chemical/chembl_21/csvdata/*.csv
#INPUT_DATASETS["datagov"]=/data/datasets/datagov/data/data/datagovdata/*.csv
#INPUT_DATASETS["mitdwh"]=/data/datasets/mitdwh/*.csv

# Generate ground truth for all CSV files in input dataset directory.
function generate_ground_truth {
  local input_csv_file=$1
  local output_csv_file=$2
  local input_csv_column_delimiter=${3-,}  # Default to comma if not available.

  args="match_regexes.py -r ${INPUT_REGEXES} -d ${input_csv_file} -c \"${input_csv_column_delimiter}\" -o ${output_csv_file}"
  echo "Running:"
  echo "python $args"
  eval python $args
  echo "Saved output as \"${output_csv_file}\"."
}

# Main.
for dataset in "${!INPUT_DATASETS[@]}";
do
  input_dataset_dir=${INPUT_DATASETS[$dataset]}
  output_csv_basename=${TODAY}-${dataset}

  for input_csv_file in ${input_dataset_dir} ;
  do
    file_basename=$(basename $input_csv_file)

    # Chemical data sets use ';' as CSV delimiter.
    if [ "${dataset}" = "chemical" ] ; then
      delimiter=";"
      generate_ground_truth ${input_csv_file} /tmp/${output_csv_basename}-${file_basename} ${delimiter}
    else
      generate_ground_truth ${input_csv_file} /tmp/${output_csv_basename}-${file_basename}
    fi
  done

done
