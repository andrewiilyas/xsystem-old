import argparse
import csv
import HTMLParser
from multiprocessing import Queue
from Queue import Empty, Queue
from threading import Thread
import pandas as pd
import requests
from lxml import html


def get_urls(csv_file):
    df = pd.read_csv(csv_file)
    urls = df['url']
    assert urls[0]
    return df


def process_url_entry(url_entry, writer, csv_fields):
    if url_entry is not None:
        url = url_entry['url']
        category = url_entry['category']

        page = requests.get(url)
        tree = html.fromstring(page.content)

        # Valid as of Oct 2016.
        regexes = tree.xpath('.//tr[@class="expression"]/*[2]')
        html_parser = HTMLParser.HTMLParser()
        for regex in regexes:
            process_regex(regex, writer, category, html_parser, csv_fields)


def process_regex(regex, writer, category, html_parser, csv_fields):
    row = {}
    for field in csv_fields:
        if field == 'category':
            row[field] = category
        if field == 'regex':
            try:
                regex_text = regex[0].text.encode('utf-8')
                unescaped_regex = html_parser.unescape(regex_text)

                # Data quality check: skip regexes that contain new lines.
                if "\n" in unescaped_regex:
                    row[field] = None
                    continue

                clean_regex = unescaped_regex.replace(" ", "")
                # Remove silly double quotes added by regex authors.
                if clean_regex.startswith('"') and clean_regex.endswith('"'):
                    clean_regex = clean_regex[1:-1]
                row[field] = clean_regex
            except:
                row[field] = None

    if row['regex'] is not None:
        try:
            writer.writerow(row)
        except UnicodeEncodeError:
            pass


def start_scrapers(urls_df, output_file):
    # Task queue for scrapers.
    url_queue = Queue(10)

    with open(output_file, 'w') as csvfile:
        csv_fields = ['category', 'regex']
        writer = csv.DictWriter(
            csvfile, fieldnames=csv_fields, quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writeheader()

        def worker():
            while True:
                # Non-blocking get.
                try:
                    url_entry = url_queue.get(False)
                except Empty:
                    url_entry = None

                if url_entry is not None:
                    process_url_entry(url_entry, writer, csv_fields)
                    url_queue.task_done()

        for i in xrange(10):
            t = Thread(target=worker)
            t.daemon = True
            t.start()

        for _, row in urls_df.iterrows():
            url_queue.put(row)

        url_queue.join()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Scrapes regexes from http://regexlib.com.')
    parser.add_argument(
        '-i',
        '--input_csv',
        help='Relative path of input CSV file containing regex '
        'category and URLs to scrape.',
        required='True')
    parser.add_argument('-o', '--output_csv',
                        help='Relative path of output CSV file containing '
                        'scraped regexes for each category.', required='True')
    args = parser.parse_args()

    print 'Scraping regexes...'

    urls_df = get_urls(args.input_csv)
    start_scrapers(urls_df, args.output_csv)

    print 'Regexes saved at "%s".' % args.output_csv
