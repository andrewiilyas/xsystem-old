# Regex Scraper Usage

The scraper.py script scrapes a list of regexes from http://www.regexlib.com .

A list of regexes is already available under regexlib_regexes.csv.  If you want to update it:

```
$ python match_regexes.py -h
usage: match_regexes.py [-h] -r REGEXES_INPUT_CSV -d DATASET_INPUT_CSV -o
                        OUTPUT_CSV [-c DATASET_COLUMN_DELIMITER]

Find candidate regexes for a given value.

optional arguments:
  -h, --help            show this help message and exit
  -r REGEXES_INPUT_CSV, --regexes_input_csv REGEXES_INPUT_CSV
                        Relative path of input CSV file containing scraped
                        regexes with labels.
  -d DATASET_INPUT_CSV, --dataset_input_csv DATASET_INPUT_CSV
                        Relative path of input dataset CSV file.
  -o OUTPUT_CSV, --output_csv OUTPUT_CSV
                        Relative path of output labels CSV file.
  -c DATASET_COLUMN_DELIMITER, --dataset_column_delimiter DATASET_COLUMN_DELIMITER
                        Character to be used as delimiter in the input dataset
                        CSV file.
```

E.g., assuming you're running it from this directory:

```
$ python scraper.py -i regexlib_urls.csv -o /tmp/regexlib_regexes.csv

# After inspecting contents of output:
$ mv /tmp/regexlib_regexes.csv regexlib_regexes.csv
```

# Regex Matcher Usage

The script match_regexes.py is used to assign labels to a column via regex matching.

Run from the current directory:

```
$ python match_regexes.py -r regexlib_regexes.csv -d "../test_cases/mit_tests/Academic_terms_all.csv" -o /tmp/out.csv
```

or specifying a column delimiter:

```
$ python match_regexes.py -r regexlib_regexes.csv -d "sample_datasets/public.binding_sites.csv" -c ";" -o /tmp/out.csv
```

The script will list a set of candidate labels based on how many regexes of that label are perfect matches for a column.
