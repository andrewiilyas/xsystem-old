import string
from random import shuffle

import distance
import editdistance
import numpy as np
from scipy.cluster import hierarchy

import rl_config


class Utils:

    @staticmethod
    def mode(lst):
        return max(set(lst), key=lst.count)

    @staticmethod
    def get_ord(symbol):
        """
            Returns a numerical value for a given character.

            The value returned is the character's offset into the class that it
            belongs to. Specifically:

            character in [:lower:] -> a = 0, ..., z = 25
            character in [:upper:] -> A = 0, ..., Z = 25
            character in [:digit:] -> 1 = 1, ..., 9 = 9
            any other character -> the decimal ASCII code
        """
        ascii_code = ord(symbol)
        # digits
        if 48 <= ascii_code <= 57:
            return int(symbol)
        # uppercase alpha characters
        elif 65 <= ascii_code <= 90:
            return ascii_code - 65
        # lowercase alpha characters
        elif 97 <= ascii_code <= 122:
            return ascii_code - 97
        return ord(symbol)

    @staticmethod
    def get_char_class(char):
        """
            Returns a short textual representation of the corresponding
            character class for a given character using the following mapping:

            character in [:lower:] -> the string "(c*)"
            character in [:upper:] -> the string "(C*)"
            character in [:digit:] -> the string "(D*)"
            character in [:punct:] -> the original character
            character in [:space:] -> the string " "
            any other character -> the original character
        """
        if char in string.ascii_lowercase:
            return "(c*)"
        elif char in string.ascii_uppercase:
            return "(C*)"
        elif char in string.digits:
            return "(D*)"
        elif char in string.whitespace:
            return " "
        elif char in string.punctuation:
            return char

    @staticmethod
    def get_class(symbol):
        ascii_code = ord(symbol)
        if 8 <= ascii_code <= 57:
            return 'D'
        elif 65 <= ascii_code <= 90:
            return 'C'
        elif 97 <= ascii_code <= 122:
            return 'c'
        return symbol

    @staticmethod
    def get_char_class_ed(char):
        if char in string.ascii_lowercase:
            return "@"
        elif char in string.ascii_uppercase:
            return "#"
        elif char in string.digits:
            return "$"
        elif char in string.whitespace:
            return "^"
        elif char in string.punctuation:
            return char

    @staticmethod
    def to_char_blocks(string):
        new_string = ""
        last_char = ""
        for x in string:
            char_type = Utils.get_char_class(x)
            if not char_type == last_char:
                last_char = char_type
                new_string += last_char
        return new_string

    @staticmethod
    def to_char_types(string):
        return "".join([Utils.get_char_class_ed(x) for x in string])

    @staticmethod
    def edit_dist_mod(str1, str2):
        new_str1 = Utils.to_char_types(str1)
        new_str2 = Utils.to_char_types(str2)
        return editdistance.eval(new_str1, new_str2)

    @staticmethod
    def col_distance(column):
        shuffle(column)
        return sum([Utils.edit_dist_mod(column[i], column[i + 1]) for i in range(len(column) - 1)]) / float(len(column) - 1)

    @staticmethod
    def has_structure(column):
        return Utils.col_distance(column) < 0.5 * sum([len(x) for x in column]) / float(len(column))

    @staticmethod
    def mode(str_list):
        max_element = None
        elements = {}
        for item in str_list:
            elements[item] = elements.get(item, 0) + 1
            if (not max_element) or (elements[item] > elements[max_element]):
                max_element = item
        return max_element

    @staticmethod
    def weighted_jaccard(str1, str2):
        d1 = distance.jaccard(str1, str2)
        d2 = 4 * \
            distance.jaccard(Utils.to_char_blocks(
                str1), Utils.to_char_blocks(str2))
        return d1 + d2 

    @staticmethod
    def cluster_strings(strings):
        def d(coord):
            i, j = coord
            return Utils.weighted_jaccard(strings[i], strings[j])
        coords = np.triu_indices(len(strings), 1)
        dist_mat = np.apply_along_axis(d, 0, coords)
        linkage_mat = hierarchy.linkage(dist_mat)
        for i in [x / 10.0 for x in range(51)]:
            fcl = hierarchy.fcluster(linkage_mat, i, "distance")
            if max(fcl) <= 5:
                return fcl

    @staticmethod
    def merge_branches(branches, func=None):
        if func is None:
            def func(x): return x
        seen = {}
        result = []
        for item in branches:
            marker = func(item)
            if marker in seen:
                for s in item.learned_lines:
                    seen[marker].learn_string(s)
            else:
                seen[marker] = item
                result.append(item)
        return result

    @staticmethod
    def perform_merge(branches, indices):
        for s in branches[indices[1]].learned_lines:
            branches[indices[0]].open_for_learning()
            branches[indices[0]].learn_string(s)
        del branches[indices[1]]
        return branches

    @staticmethod
    def compare_dafsas_flat(d1, d2, mod=False, accuracy=1):
        n = rl_config.config["clt_sample_size"]
        curr_stdev = None
        inc = 10
        all_dists = []
        # pep8: use function instead
        needed_sample_size = lambda x: (1.96 * x / float(accuracy))**2
        while (curr_stdev is None or len(all_dists) < needed_sample_size(curr_stdev)):
            for i in range(inc):
                g1 = d1.generate_random_strings(n)
                g2 = d2.generate_random_strings(n)
                if not mod:
                    all_dists.append(
                        np.mean([editdistance.eval(g1.next(), g2.next()) for i in range(n)]))
                else:
                    all_dists.append(
                        np.mean([Utils.edit_dist_mod(g1.next(), g2.next()) for i in range(n)]))
            curr_stdev = np.std(all_dists)
        return np.mean(all_dists)

    @staticmethod
    def compare_dafsas_nested(d1, d2):
        n = rl_config.config["clt_sample_size"]
        curr_stdev = 5
        all_dists = []
        needed_sample_size = lambda x: (1.96 * x / 0.5)**2
        while len(all_dists) < needed_sample_size(curr_stdev):
            all_dists += [d1.feed_forward_score([s]) for s in d2.generate_random_strings(n)]
            curr_stdev = np.std(all_dists)
        return np.mean(all_dists)

    @staticmethod
    def compare_dafas_merging(d1, d2):
        scores = Utils.compare_dafsas_nested(d1, d2)
        return scores


    @staticmethod
    def two_dimensional_min(mat):
        min_index = (0, 0)
        min_val = -1
        for i in range(len(mat)):
            for j in range(len(mat[i])):
                if mat[i][j] < min_val or min_val == -1:
                    min_index = (i, j)
                    min_val = mat[i][j]
        return min_index

    @staticmethod
    def g(branches, i): 
        return branches[i].generate_random_strings(1).next()

    @staticmethod
    def condense_branches(branches, min_distance, concentrations):
        def distance_func((x, y)): return Utils.compare_dafas_merging(branches[x], branches[y]) #editdistance.eval(Utils.g(branches, x), Utils.g(branches, y))
        flag = True
        while flag:
            flag = False
            for i in range(len(branches)):
                for j in range(len(branches)):
                    dist = distance_func((i, j))
                    if not i == j and dist < min_distance:
                        branches = Utils.perform_merge(branches, (i, j))
                        concentrations[i] += concentrations[j]
                        del concentrations[j]
                        flag = True
                        break
                if flag:
                    break
        return branches, concentrations

    @staticmethod
    def merge_branches_dynamic(branches, concentrations):
        def distance_func((x, y)): return Utils.compare_dafas_merging(branches[x], branches[y]) #editdistance.eval(Utils.g(branches, x), Utils.g(branches, y))
        distance_matrix = [
            [distance_func((i, j)) if not i == j else 10 for i in range(len(branches))]
            for j in range(len(branches))
        ]
        min_index = Utils.two_dimensional_min(distance_matrix)
        min_distance = distance_func(min_index)
        branches = Utils.perform_merge(branches, min_index)
        concentrations[min_index[0]] += concentrations[min_index[1]]
        del concentrations[min_index[1]]
        return branches, min_distance, concentrations
