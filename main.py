import argparse
import sys
import time
import traceback
from functools import wraps
from os import listdir

import editdistance
import numpy as np
import pandas as pd

import rl_config
from representation_learner import RepresentationLearner
from utils import Utils


def fn_timer(function):
    """
    Timer decorator that prints total running time of execution.

    A function decorated by this may also provide additional stats via
    a "additional_timer_stats" kwarg.
    """
    @wraps(function)
    def function_timer(*args, **kwargs):
        t0 = time.time()
        result = function(*args, **kwargs)
        t1 = time.time()

        # First column is time.
        stats_csv_header = 'function,runtime_seconds'
        stats_csv_data = '%s,%s' % (function.func_name, str(t1 - t0))

        # Remaining columns.
        if 'additional_timer_stats' in kwargs:
            for k, v in kwargs['additional_timer_stats'].iteritems():
                stats_csv_header += ',%s' % k
                stats_csv_data += ',%s' % str(v)

        if 'additional_timer_stats_from_result' in kwargs:
            for stat in kwargs['additional_timer_stats_from_result']:
                stats_csv_header += ',%s' % stat
                stats_csv_data += ',%s' % str(result[stat])

        print(stats_csv_header)
        print(stats_csv_data)

        return result
    return function_timer


def get_files_from_path(path):
    """
    Retrieves all non-hidden files from a path.
    """
    visible_files = []
    for f in listdir(path):
        if not f.startswith('.'):  # filter out hidden files
            path_to_file = path + "/" + f
            visible_files.append(path_to_file)
    return visible_files


def get_columns_from_file(filename, dataframe=None):
    """
    Given the absolute path of a file it retrieves the column names.
    It assumes these appear in the first line (CSV, TSV).
    """
    column_names = []
    if dataframe is None:
        csv = pd.read_csv(filename, dtype=str, error_bad_lines=False) \
                .replace([None], [""], regex=True)
        column_names = list(csv)
    else:
        column_names = list(dataframe)
    return column_names


def get_col_from_file(fname, col):
    if col is None:
        l = list([x.replace('\n', '') for x in open(fname).readlines()])
    else:
        l = list(pd.read_csv(fname, dtype=str, error_bad_lines=True)
                 [col].replace([None], [""], regex=True))
    return [str(x) for x in l]


def get_regex_from_file(fname, col=None):
    l = get_col_from_file(fname, col)
    model = learn_model(l)
    return str(model['model'])


def learn_model(data):
    """
    Given an iterable of data values it learns a model that represents them
    """
    r = RepresentationLearner(data)
    r.learn()

    num_branches = r.get_num_branches()
    total_num_hinges = r.get_total_num_hinges()
    max_num_hinges = r.get_max_num_hinges()
    avg_num_hinges = total_num_hinges / num_branches

    return {'model': r,
            'num_branches': num_branches,
            'max_num_hinges': max_num_hinges,
            'avg_num_hinges': avg_num_hinges}


def compare_regex_from_file(fname1, fname2=None, col1=None, col2=None):
    l1 = get_col_from_file(fname1, col1)
    l2 = get_col_from_file(fname2, col2)

    r1 = RepresentationLearner(l1)
    r1.learn()

    r2 = RepresentationLearner(l2)
    r2.learn()

    return r1.score_strings_from_model(r2), r2.score_strings_from_model(r1)


def compare_dafsas_flat(d1, d2, mod=False, accuracy=0.1):
    return Utils.compare_dafsas_flat(d1, d2, mod, accuracy)


def compare_representation_flat(fname1, fname2, col1, col2, mod=False):
    l1 = get_col_from_file(fname1, col1)
    l2 = get_col_from_file(fname2, col2)

    r1 = RepresentationLearner(l1)
    r1.learn()

    r2 = RepresentationLearner(l2)
    r2.learn()
    rep1results = []
    for cluster in r1.clusters:
        rep1results.append(
            min([compare_dafsas_flat(cluster, x, mod) for x in r2.clusters]))

    rep2results = []
    for cluster in r2.clusters:
        rep2results.append(
            min([compare_dafsas_flat(cluster, x, mod) for x in r1.clusters]))

    return np.mean(rep1results), np.mean(rep2results)


def format_for_output(filename, colname, structuredness_score, model_string, samples):
    sample_string = ','.join(str(v) for v in samples)
    string = (filename + " - " + colname + '\n' + "STRUCT. SCORE: " +
              str(structuredness_score) + '\n' + " MODEL: " + model_string +
              '\n' + " Samples: " + sample_string + "\n\n")
    return string


@fn_timer
def learn_structures_for_single_column(column_data, *args, **kwargs):
    """
    Learns the structures model for a single column.

    **kwargs here are only used for passing additional_timer_stats to fn_timer decorator.
    """
    try:
        model = learn_model(column_data)
    except BaseException as e:
        traceback.print_exc()
        model['model'] = "ERROR: " + str(e)
        model['avg_num_hinges'] = 0
        model['max_num_hinges'] = 0
        model['num_branches'] = 0
    return model


def learn_structures_for_all_columns_in_file(filename, should_log=True):
    """
    Learns the structures model for all columns in a file.
    """
    dataframe = pd.read_csv(filename, dtype=str, error_bad_lines=False).replace(
        [None], ["None"], regex=True)
    colnames = get_columns_from_file(filename, dataframe)
    models = []
    for column in colnames:
        column_data = dataframe[column]
        average_row_length = get_average_row_length(column_data)
        structuredness_score = Utils.has_structure(column_data[:200])
        model = learn_structures_for_single_column(
            column_data,
            additional_timer_stats={
                'average_row_length': average_row_length,
                'column_length': len(column_data)
            },
            additional_timer_stats_from_result=[
                'avg_num_hinges', 'max_num_hinges', 'num_branches']
        )
        string = format_for_output(filename,
                                   column,
                                   structuredness_score,
                                   str(model['model']),
                                   column_data[:5])
        models.append(model['model'])
        # Consider making this optional when the outer function is timed, as I/O
        # affects total execution time.
        #
        # TODO: Use a logger instead.
        # Quick hack fix:
        if should_log:
            print(str(string))
    return models, dataframe

def error_detect_for_filename(column_filename, output_filename, threshold=0.5):
    """
    Given a columnn file name, we learn a model to the column, then fit each entry into the model,
    and output rows detected to be outliers from the column
    """
    models, _ = learn_structures_for_all_columns_in_file(column_filename, should_log=False)
    dataframe = pd.read_csv("sample_outlier_indexed.csv", dtype=str)
    dataframe["Indexer"] = dataframe["Indexer"].astype(int)
    dataframe = dataframe.set_index("Indexer")
    dataframe = dataframe.sort_index()
    #dataframe = dataframe.reset_index()
    #dataframe = dataframe.drop("Index")
    score_vector = []
    all_scores = []
    raw_scores = []
    print map(str, models)
    for index, row in dataframe.iterrows():
        row_scores = []
        for i in range(len(row)):
            elem = row[i]
            model = models[i]
            score = model.word_outlier_score(elem)
            row_scores.append(score)
        if len(all_scores) > 0:
            all_scores.append(row_scores)
#            all_scores.append(row_scores-np.min(raw_scores, axis=0))
#            all_scores.append(row_scores-np.array(raw_scores[-1]))
        else:
            all_scores.append(np.array([0]*len(row_scores)))
        raw_scores.append(row_scores)
    all_scores_transposed = zip(*all_scores)
    for i in range(len(all_scores_transposed)):
        dataframe["col" + str(i+1) + "-score"] = all_scores_transposed[i]

    print len(zip(*all_scores))
    all_scores_thresholded = np.array(all_scores)
    #all_scores_thresholded[all_scores_thresholded <= threshold] = 0
    #all_scores_thresholded[all_scores_thresholded > threshold] = 1
    generated_labeled = pd.DataFrame(all_scores_thresholded)
    generated_labeled.to_csv(output_filename + ".labeled", header=False, index=False)
    dataframe.to_csv(output_filename)

@fn_timer
def learn_structures_for_all_files_in_path(path):
    """
    Given an input path (path), it reads all files in the directory, all columns
    of every file and learn the structure per column.
    """
    filenames = get_files_from_path(path)
    for f in filenames:
        learn_structures_for_all_columns_in_file(f)


def get_average_row_length(column):
    row_lengths = [len(row) for row in column]
    return 0 if len(row_lengths) == 0 else (float(sum(row_lengths)) / len(row_lengths))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Learn structures model for a single file, or all files in a directory.")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-f', '--filename',
                       help='Relative path of input filename to be processed.')
    group.add_argument(
        '-d', '--directory', help='Relative path of input directory containing all files to be processed.')
    group.add_argument(
        '-e', '--errordetect', help='Print out an error vector of guessed errors')
    parser.add_argument('-o', '--output', help='Where to output error detection log (csv preferred)')
    parser.add_argument('-t', '--threshold', help='Threshold for calling a column an error (ignored unless used with -e)')
    args = parser.parse_args()

    # Either one of the two is always provided.
    if args.directory is not None:
        learn_structures_for_all_files_in_path(args.directory)
    elif args.filename is not None:
        learn_structures_for_all_columns_in_file(args.filename)
    else:
        error_detect_for_filename(args.errordetect, args.output, threshold=float(args.threshold))
