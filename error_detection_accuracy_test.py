import argparse
import numpy as np
import rstr
from random import shuffle
from main import learn_model
import string
from numpy.random import choice
from random import choice as rand_choice
import re
import matplotlib.pyplot as plt

def random_filler(N):
    return ''.join(rand_choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(N))

def randomly_mutate(string, n_bits):
    filler = random_filler(n_bits)
    indices_to_replace = set(choice(len(string), n_bits, replace=False))
    new_string = ""
    j = 0
    for i in range(len(string)):
        if i in indices_to_replace:
            new_string += filler[j]
            j+=1
        else:
            new_string += string[i]
    return new_string

def error_detection_experiment(n, base, r, m=2):
    unshuffled_regex = [rstr.xeger(base) for i in range(n-int(n*r))] + [randomly_mutate(rstr.xeger(base), 2) for i in range(int(n*r))]
    shuffle(unshuffled_regex)
    m = learn_model(unshuffled_regex)["model"]
    re_pattern = re.compile(base)
    tp = 0.0
    tn = 0.0
    fp = 0.0
    fn = 0.0
    scores = [m.score_strings([s]) for s in unshuffled_regex]
    for i in range(len(unshuffled_regex)):
        if not re_pattern.match(unshuffled_regex[i]):
            if scores[i] > 0.4:
                tp += 1
            else:
                fn += 1
        else:
            if scores[i] > 0.4:
                fp += 1
            else:
                tn += 1
    return 2*tp/(2*tp+fn+fp)

def full_error_detect(n, base):
    for m in range(5):
        x = np.array(range(19))/20.0 + 0.05
        y = [error_detection_experiment(n, base, r,m+1) for r in x]
        plt.plot(x, y)
    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates accuracy plots')
    parser.add_argument('base_regex', metavar="b", type=str, help="Baseline regex (hope to capture this)")
    parser.add_argument('--num_samples', metavar="n", type=int, help="Number of samples to generate")
    args = parser.parse_args()
    n = args.num_samples if args.num_samples else 100
    full_error_detect(n, args.base_regex)
