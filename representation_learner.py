from random import random

import numpy as np
import numpy.random as rand

import rl_config
from structure_learner import StructuredTextLearner
from utils import Utils


class RepresentationLearner:

    def __init__(self, lines):
        self.lines = lines
        rand.shuffle(self.lines)
        if rl_config.config["should_branch"]:
            self.clusters = [StructuredTextLearner([self.lines[0]])]
            self.branching_threshold = rl_config.config[
                "branching_seed_threshold"]
        else:
            self.string_clusters = self.cluster()
            self.clusters = [StructuredTextLearner(
                c) for c in self.string_clusters]
        self.cluster_concentrations = [1]

    # Used for performance plots.
    def get_num_branches(self):
        return len(self.clusters)

    # Used for performance plots.
    def get_total_num_hinges(self):
        num_hinges = 0
        for cluster in self.clusters:
            num_hinges += cluster.get_num_hinges()
        return num_hinges

    # Used for performance plots.
    def get_max_num_hinges(self):
        max_num_hinges = 0
        for cluster in self.clusters:
            max_num_hinges = max(max_num_hinges, cluster.get_num_hinges())
        return max_num_hinges

    def cluster(self):
        sample_size = rl_config.get("default_sample_size")(self.lines)
        seeds = Utils.cluster_strings(self.lines[:sample_size])
        clusters = [[] for _ in range(max(seeds))]
        for i in range(len(seeds)):
            clusters[seeds[i] - 1].append(self.lines[i])
        return clusters

    def get_right_cluster(self, line):
        if rl_config.config["should_branch"]:
            return self.get_right_or_branch(line)
        return self.get_right_no_branch(line)

    def get_right_no_branch(self, line):
        min_s = None
        min_c = None
        for i in range(len(self.clusters)):
            s = self.clusters[i].feed_forward_score([line])
            if s == 0:
                return i
            if min_s is None or s < min_s:
                min_s = s
                min_c = i
        return min_c

    def get_right_or_branch(self, line):
        min_s = None
        min_c = None
        if len(self.clusters) > rl_config.config["max_branches"]:
            self.clusters, self.branching_threshold, self.cluster_concentrations = Utils.merge_branches_dynamic(
                self.clusters, self.cluster_concentrations)
        for i in range(len(self.clusters)):
            s = self.clusters[i].feed_forward_score([line])
            if s == 0:
                return i
            if (min_s is None or s < min_s) and s < self.branching_threshold:
                min_s = s
                min_c = i
        if min_c is None:
            self.clusters.append(StructuredTextLearner([line]))
            self.cluster_concentrations.append(0)
            return len(self.clusters) - 1
        else:
            return min_c

    def learn(self):
        [s.learn_deep_structure() for s in self.clusters]
        for line in self.lines:
            x = self.get_right_cluster(line)
            self.clusters[x].learn_string(line)
            self.cluster_concentrations[x] += 1
        self.clusters, self.cluster_concentrations = Utils.condense_branches(self.clusters, 0.1, self.cluster_concentrations)

    def __str__(self):
        assert len(self.clusters) == len(self.cluster_concentrations)
        x = list(set([str(x) for x in self.clusters]))
        return "|".join(x)

    def generate_strings(self, sample_size):
        for i in range(len(self.clusters)):
            for gen_string in self.clusters[int(random() * len(self.clusters))].generate_random_strings(sample_size):
                yield gen_string
    
    def generate_mh_strings(self, num_strings=None):
        k = 0
        generators = [c.generate_mh_strings() for c in self.clusters]
        while num_strings is None or k < num_strings:
            yield generators[k%len(generators)].next()
            k += 1

    def word_outlier_score(self, word):
        #right_cluster = self.get_right_no_branch(word)
        total_score = 0
        for right_cluster in range(len(self.clusters)):
            raw_score = self.clusters[right_cluster].feed_forward_score([word])
            adjustment = self.cluster_concentrations[right_cluster]/float(sum(self.cluster_concentrations))
            adjusted_score = adjustment*raw_score
            print word, raw_score, adjustment
            total_score += adjusted_score
        print word, total_score/float(len(self.clusters))
        return total_score/float(len(self.clusters))

    def score_strings(self, strings):
        strings_list = [string for string in strings]
        x = [self.clusters[self.get_right_no_branch(string)].feed_forward_score([
            string]) for string in strings_list]
        return 100 * float(sum(x)) / sum([len(string) for string in strings_list])

    def score_strings_from_model(self, model):
        n = rl_config.config["clt_sample_size"]
        curr_stdev = 5
        all_dists = []
        needed_sample_size = lambda x: (1.96 * x / 0.5)**2
        while len(all_dists) < needed_sample_size(curr_stdev):
            all_dists += [self.score_strings(model.generate_strings(n))]
            curr_stdev = np.std(all_dists)
        return np.mean(all_dists)
