import math

config = {
    # Actual Configurables
    "capture_percentage": 0.65,
    "default_sample_size": (lambda x: min(len(x), 50 * int(math.log(len(x))))),
    "max_branches": 10,
    "branching_seed_threshold": 0.5,
    # Recommended no change
    "should_branch": True,
    "clt_sample_size": 30
}


def get(key):
    return config.get(key, None)
